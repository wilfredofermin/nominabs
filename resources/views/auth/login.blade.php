@extends('layouts.app')
@section('content')
    <div class="container" xmlns="http://www.w3.org/1999/html">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div align="center" class="panel-heading"><h5>SISTEMA DE NOMINA</h5> </div>
                    <br>
                         <div align="center" class="center-block"><img src="img/logologin.png" class="img-rounded" alt="logo-app" width="257" height="260">
                         </div>
                            <br>
                                <div class="panel-body">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('auth.login') }}">
                                        {{ csrf_field() }}
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <div class="col-md-8 col-md-offset-2">
                                                <div class="input-group">
                                                <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-user"></i>
                                             </span>
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Correo electronico">
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-lock"></i>
                                    </span>
                                        <input id="password" type="password" maxlength="5"  class="form-control " name="password" placeholder="password">
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <input type="submit" class="btn btn-lg btn-primary btn-block" value="Acceder" id="a">
                                        <div align="center"><a class="btn btn-link" href="{{ url('/password/reset') }}">
                                            Olvidaste tu clave?</a>
                                        </div>
                                    <div align="center" class="center-block"><h6>&reg Body Shop Athletic Club</h6></div>
                                    <div align="center"> <a  class="btn btn-link" href="{{ url('https://about.me/wilfredofermin') }}">
                                            <font size="1"> Wilfredo Fermin Aplicaciones &copy 2016  </font>
                                        </a></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
